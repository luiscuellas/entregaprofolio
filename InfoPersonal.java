package com.example.entregaportfolio.Modelo;

import com.example.entregaportfolio.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import org.springframework.data.annotation.Id;

@Entity
public class InfoPersonal {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private int ID;
    private String NombrePer;
    private String ApellidoPer;


    public InfoPersonal(String firstName, String lastName) {
        this.NombrePer = firstName;
        this.ApellidoPer = lastName;
    }

    @Override
    public String toString() {
        return String.format(
                "InfoPersonal[id=%d, Nombre='%s', Apellido='%s']",
                ID, NombrePer, ApellidoPer);
    }

    public int getID() {
        return ID;
    }

    public String getNombrePer() {
        return NombrePer;
    }

    public String getApellidoPer() {
        return ApellidoPer;
    }
}
