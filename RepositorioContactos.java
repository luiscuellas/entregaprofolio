package com.example.entregaportfolio.Repositorio;

import com.example.entregaportfolio.Modelo.Contactos;

public interface RepositorioContactos {
    public Contactos findById(int id);

    public Contactos findByNombre(String Nombre);

    public Contactos findByTel(int Telefono);

    public Contactos findByEmail(String Email);
}
