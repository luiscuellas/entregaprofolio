package com.example.entregaportfolio.Repositorio;

import com.example.entregaportfolio.Modelo.Lenguaje;

public interface RepositorioLenguaje {

    public Lenguaje findById(int id);

    public Lenguaje findByNombre(String NombreLen);

    public Lenguaje findByNivel(String Nivel);

}
