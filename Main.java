import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/contactos")
public class ContactoController {

        @Autowired
            private ContactoService contactoService;

                @GetMapping
                    public List<Contacto> listarContactos() {
                                return contactoService.listarContactos();
                    }

                        @PostMapping
                            public Contacto añadirContacto(@RequestBody Contacto contacto) {
                                        return contactoService.añadirContacto(contacto);
                            }

                                @PutMapping("/{id}")
                                    public Contacto editarContacto(@PathVariable Long id, @RequestBody Contacto contactoDetalles) {
                                                return contactoService.editarContacto(id, contactoDetalles);
                                    }

                                        @DeleteMapping("/{id}")
                                            public ResponseEntity<Void> borrarContacto(@PathVariable Long id) {
                                                        contactoService.borrarContacto(id);
                                                                return ResponseEntity.noContent().build();
                                            }
}
                                            }
                                    }
                            }
                    }
    @RestController
    @RequestMapping("/api/proyectos")
    public class ProyectoController {

        @Autowired
            private ProyectoService proyectoService;

                @GetMapping
                    public List<Proyecto> listarProyectos() {
                            return proyectoService.listarProyectos();
                                }

                                    @PostMapping
                                        public Proyecto añadirProyecto(@RequestBody Proyecto proyecto) {
                                                return proyectoService.añadirProyecto(proyecto);
                                                    }

                                                        @PutMapping("/{id}")
                                                            public Proyecto editarProyecto(@PathVariable Long id, @RequestBody Proyecto proyectoDetalles) {
                                                                    return proyectoService.editarProyecto(id, proyectoDetalles);
                                                                        }

                                                                            @DeleteMapping("/{id}")
                                                                                public ResponseEntity<Void> borrarProyecto(@PathVariable Long id) {
                                                                                        proyectoService.borrarProyecto(id);
                                                                                                return ResponseEntity.noContent().build();
                                                                                                    }
                                                                                            @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    private String nombre;
    private String descripcion;
    private String dominio;

    // Getters y Setters

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDominio() {
        return dominio;
    }

    public void setDominio(String dominio) {
        this.dominio = dominio;
    }
}