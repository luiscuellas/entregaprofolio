package com.example.entregaportfolio.Modelo;

import com.example.entregaportfolio.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import org.springframework.data.annotation.Id;

@Entity
public class Lenguaje {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String NombreLen;
    private String Nivel;

    public String toString() {
        return String.format(
                "Lenguaje[id=%d, NombreLen='%s', Nivel='%s']",
                id, NombreLen, Nivel);
    }

    public int getId() {
        return id;
    }

    public String getNombreLen() {
        return NombreLen;
    }

    public String getNivel() {
        return Nivel;
    }
}