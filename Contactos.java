package com.example.entregaportfolio.Modelo;

import com.example.entregaportfolio.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import org.springframework.data.annotation.Id;

@Entity
public class Contactos {


    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private static int id;
    private String Nombre;
    private String Apellido;
    private int Telefono;
    private String Email;
    private String nombre;


    public Contactos(String firstName, String lastName) {
        this.Nombre = firstName;
        this.Apellido = lastName;
    }

    public Contactos() {

    }

    public static void deleteCont(int id) {
    }

    @Override
    public String toString() {
        return String.format(
                "Contacto[id=%d, Nombre='%s', Apellido='%s', Telefono='%d', Email='%s']",
                id, Nombre, Apellido);
    }

    public static int getId() {
        return id;
    }

    public String getNombre() {
        return Nombre;
    }

    public String getApellido() {
        return Apellido;
    }

    public int getTelefono() {
        return Telefono;
    }

    public String getEmail() {
        return Email;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setTelefono(String s) {
    }
}