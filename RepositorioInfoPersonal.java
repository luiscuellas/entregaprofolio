package com.example.entregaportfolio.Repositorio;

import com.example.entregaportfolio.Modelo.InfoPersonal;
import org.springframework.stereotype.Repository;

@Repository
public interface RepositorioInfoPersonal {

    public InfoPersonal findById(int id);

}
