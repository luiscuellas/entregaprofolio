package com.example.entregaportfolio.Repositorio;

import com.example.entregaportfolio.Modelo.Lenguaje;
import com.example.entregaportfolio.Modelo.Proyectos;

import java.util.Date;

public interface RepositorioProyectos {

    public Proyectos findById(int id);

    public Proyectos findByNombre(String Nombre);

    public Proyectos findByDate(Date Fecha);

    public Proyectos findByLenguaje(Lenguaje lenguajes);
}
