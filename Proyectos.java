package com.example.entregaportfolio.Modelo;

import com.example.entregaportfolio.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.ManyToMany;
import org.springframework.data.annotation.Id;

import java.util.Date;
import java.util.List;

@Entity
public class Proyectos {
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        private int id;
        private String Nombre;
        private Date Fecha;
        @ManyToMany
        private List<Lenguaje> lenguajes;

    public String toString() {
        return String.format(
                "Proyecto[id=%d, Nombre='%s', Fecha='%s']",
                id, Nombre, Fecha);
    }
    public int getId() {
        return id;
    }

    public String getNombre() {
        return Nombre;
    }

    public Date getFecha() {
        return Fecha;
    }

    public List<Lenguaje> getLenguajes() {
        return lenguajes;
    }
}
